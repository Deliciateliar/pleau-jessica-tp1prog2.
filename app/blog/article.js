class Article{
    constructor(id, sujet, titre){
        this.id = id;
        this.sujet = sujet;
        this.titre = titre;    
    }
}

const data = [
    new Article("0", "first","First"),
    new Article("1", "second","Second"),
    new Article("2", "third","Third"),
    new Article("3", "fourth","Fourth"),
    new Article("4", "fifth","Fifth"),
    new Article("5", "sixth","Sixth"),
    new Article("6", "seventh","Seventh"),
    new Article("7", "eight","Eight"),
    new Article("8", "ninth","Ninth"),
    new Article("9", "tenth","Tenth"),
];

module.exports = {
    getAllArticles() {
        return data;
    },

    getArticleById(id) {
        return data.find(function (article) {
            return article.id === id;
        });
    }
}