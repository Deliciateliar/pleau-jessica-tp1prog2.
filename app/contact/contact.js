class Contact {
    constructor(
        id,
        prenom,
        nom,
        courriel,
        phone,
        sujet,
        demande,
    ){
        this.id = id;
        this.prenom = prenom;
        this.nom = nom;
        this.courriel = courriel;
        this.phone = phone;
        this.sujet = sujet;
        this.demande = demande;
    }
}

const data = [
];

module.exports = {
    getAllContacts() {
        return data;
    },

    addContact(
        prenom,
        nom,
        courriel,
        phone,
        sujet,
        demande,
    ) {
        data.push(
            new Contact(
                data.length,
                prenom,
                nom,
                courriel,
                phone,
                sujet,
                demande,
            )
        )
    }
}