var express = require('express');
var exphbs  = require('express-handlebars');
const { getAllArticles, getArticleById } = require('./blog/article');
const { addContact, getAllContacts } = require('./contact/contact');
const { getAllFormations, getFormationById, getFormationsForType} = require('./formations/formationdb')
var app = express();

app.engine('.hbs', exphbs({extname: '.hbs'}));
app.set('view engine', '.hbs');

app.use(express.urlencoded({
    extended: true
}))

app.get('/accueil', (req, res, next) => {
    res.render('accueil', { title: 'Libri' });
});

app.get('/nosformations', (req, res, next) => {
    const formation = getAllFormations();
    res.render('nosformations', { 
        title:'nosformations',
        description: 'formations',
        formation,
    });
});

app.get('/nosformations/:type', (req, res, next) => {
    const formations = getFormationsForType(req.params.type);
    console.log(formations)
    res.render('formations-for-type', {
        type: req.params.type,
        formations
    });
});

app.get('/nosformations/:type/:id', (req, res, next) => {
    const formation = getFormationById(req.params.id);
    res.render('formation', {
        formation,
    });
})

app.get('/blog', (req, res, next) => {
    const articles = getAllArticles();
    res.render('blog', { 
        title:'blog',
        description: 'Ceci est une liste d\'articles',
        articles,
    });
});

app.get('/article/:id', (req, res, next) => {
    const article = getArticleById(req.params.id);
    res.render('article', {
        article,
    });
})

app.get('/contact', (req, res, next) => {
    res.render('contact');
});

app.post('/contact', (req, res, next) => {
    if (
        req.body.fname.trim() === "" ||
        req.body.lname.trim() === "" ||
        req.body.phone.trim() === ""
    ) {
       res.render('contact-error') 
    } else {
        addContact(
            req.body.fname,
            req.body.lname,
            req.body.courriel,
            req.body.phone,
            req.body.sujet,
            req.body.demande,
        )
        res.render('contact-success');
    }
    
})

app.get('/article', (req, res, next) => {
    res.render('article');
});

app.listen(3000);

module.exports = app;