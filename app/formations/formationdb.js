class Formation{
    constructor(id, nom, description, price, type){
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.price = price;
        this.type = type;    
    }
}

const data = [
    new Formation ("0", "1ère formation MongoDB","Débutant", "$42,69", "mongo"),
    new Formation ("1", "2ème formation MongoDB","Débutant 2", "$42,69", "mongo"),
    new Formation ("2", "3ème formation MongoDB","Intermédiaire", "$42,69", "mongo"),
    new Formation ("3", "4ème formation MongoDB","Intermédiaire 2", "$42,69", "mongo"),
    new Formation ("4", "5ème formation MongoDB","Expert", "$42,69", "mongo"),
    new Formation ("5", "1ère formation Node.js","Débutant", "$42,69", "node"),
    new Formation ("6", "2ème formation Node.js","Débutant 2", "$42,69", "node"),
    new Formation ("7", "3ème formation Node.js","Intermédiaire", "$42,69", "node"),
    new Formation ("8", "4ème formation Node.js","Intermédiaire 2", "$42,69", "node"),
    new Formation ("9", "5ème formation Node.js","Expert", "$42,69", "node"),
];

module.exports = {
    getAllFormations() {
        return data;
    },

    getFormationsForType(type) {
        return data.filter((formation) => formation.type === type)
    },

    getFormationById(id) {
        return data.find(function (formation) {
            return formation.id === id;
        });
    }
}